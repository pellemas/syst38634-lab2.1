package password;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * @author Mason Pelletier 991524171
 * 
 * This class validated passwords and it will be developed using TDD
 */

public class PasswordValidatorTest {
	
	@Test
	public void testIsValidLengthRegular() {
		assertTrue("Invalid password length", PasswordValidator.isValidLength("1234567890"));
	}
	
	@Test
	public void testIsValidLengthException() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength(""));
	}
	
	@Test
	public void testIsValidLengthExceptionSpaces() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength("        "));
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		assertTrue("Invalid password length", PasswordValidator.isValidLength("12345678"));
	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength("1234567"));
	}
	
	@Test
	public void testHasEnoughDigitsRegular() {
		assertTrue("Not enough digits", PasswordValidator.hasEnoughDigits("a123456789"));
	}
	
	@Test
	public void testHasEnoughDigitsException() {
		assertFalse("Not enough digits", PasswordValidator.hasEnoughDigits("        "));
	}
	
	@Test
	public void testHasEnoughDigitsBoundaryIn() {
		assertTrue("Not enough digits", PasswordValidator.hasEnoughDigits("abc12def"));
	}
	
	@Test
	public void testHasEnoughDigitsBoundaryOut() {
		assertTrue("Not enough digits", PasswordValidator.hasEnoughDigits("abc1defg"));
	}
	
	@Test
	public void testHasEnoughUpperLowerRegular() {
		assertTrue("Not enough upper and lower case", PasswordValidator.hasEnoughUpperLower("abAB567890"));
	}
	
	@Test
	public void testHasEnoughUpperLowerException() {
		assertTrue("Not enough upper and lower case", PasswordValidator.hasEnoughUpperLower("aA        "));
	}
	
	@Test
	public void testHasEnoughUpperLowerBoundaryIn() {
		assertTrue("Not enough upper and lower case", PasswordValidator.hasEnoughUpperLower("aB34567890"));
	}
	
	@Test
	public void testHasEnoughUpperLowerBoundaryOut() {
		assertTrue("Not enough upper and lower case", PasswordValidator.hasEnoughUpperLower("1234567890"));
	}
	

}
