package password;

/*
 * @author Mason Pelletier 991524171
 * 
 * This class validated passwords and it will be developed using TDD
 */

public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	private static int MIN_NUM_DIGITS = 2;

	public static boolean isValidLength ( String password) {
		if (password != null) {
		return password.trim().length() >= MIN_LENGTH;
		}
		return false;
	}
	
	public static boolean hasEnoughDigits(String password) {
		int numDigits = 0;
		int length = password.trim().length();
		for (int i = 0; i < length; i++ ) {
			if (Character.isDigit(password.charAt(i)));
			numDigits++;
		}
		return numDigits >= 2;
	}
	
	public static boolean hasEnoughUpperLower(String password) {
		int upperCase = 0;
		int lowerCase = 0;
		int length = password.trim().length();
		for (int i = 0; i < length; i++ ) {
			if (Character.isUpperCase(password.charAt(i)));
			upperCase++;
			if (Character.isLowerCase(password.charAt(i)));
			lowerCase++;
		}
		return upperCase > 0 && lowerCase > 0;
		
	}
	
}
